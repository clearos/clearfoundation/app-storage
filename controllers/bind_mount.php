<?php

/**
 * Bind Mount controller.
 *
 * @category   apps
 * @package    storage
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Bind Mount controller.
 *
 * @category   apps
 * @package    storage
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

class Bind_Mount extends ClearOS_Controller
{
    /**
     * Bind mount mappings overview.
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage');
        $this->load->library('storage/Storage_Device');

        // Load view data
        //---------------

        try {

            $data['bind_mapping_details'] = $this->storage_device->get_bind_mount_mapping_details();

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load the views
        //---------------

        $this->page->view_form('bind_mount/summary', $data, lang('storage_mappings'));
    }

    /**
     * Bind mount detail view.
     *
     * @param int $source_index encoded source index
     *
     * @return view
     */

    function view($source_index)
    {
        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage');
        $this->load->library('storage/Storage_Device');

        // Load view data
        //---------------

        try {
            $data['source'] = base64_decode(strtr($source_index, '-_.', '+/='));
            $data['details'] = $this->storage_device->get_mapping_details($data['source']);

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }


        // Load the views
        //---------------

        $this->page->view_form('bind_mount/item', $data, lang('storage_store'));
    }

    /**
    * Bind subdirectory
    *
    * @return View
    * @throws Engine_Exception
    */

    function bind_mount_directories()
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');

        try {
            
            if ($_POST) {
                $source_mount_point = $this->input->post('source_mount_point');
                $target_folder = $this->input->post('target_folder');

                // Bind Mount
                //-----------
                $this->storage_device->do_bind_mount_subdirectories($source_mount_point, $target_folder);
                redirect('/storage/');

            }

        } catch (Exception $e) { 
            $this->page->view_exception($e);
            return;
        }

        // View
        //........

        $this->page->view_form('bind_mount/bind_folder');
    }

    /**
    * Unmount subdirctories View
    *
    * @param init $source_index source index
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function unmount($source_index)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');

        $data['source'] = base64_decode(strtr($source_index, '-_.', '+/='));

        $mapping =  $this->storage_device->get_mapping_details($data['source']);
        $subdirectory = $mapping['target_dir'];


        // confirm uri
        //---------------
        $confirm_uri = '/app/storage/bind_mount/unmount_subdirectory/' . $source_index;
        $cancel_uri = '/app/storage/';

        $this->page->view_confirm(lang('storage_confirm_unmount_subdirectory') . "<b> $subdirectory </b>?", $confirm_uri, $cancel_uri);
    }

    /**
    * Unmount subdirectory
    *
    * @param init $source_index source index
    *
    * @return redirect 
    * @throws Engine_Exception
    */

    function unmount_subdirectory($source_index)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');

        $data['source'] = base64_decode(strtr($source_index, '-_.', '+/='));

        try {
            $mapping =  $this->storage_device->get_mapping_details($data['source']);
            $target_folder = $mapping['target_dir'];
            $source_mount_point = $mapping['source_dir'];

            // Bind Mount
            //-----------
            $this->storage_device->do_unmount_subdirectories($source_mount_point, $target_folder);
            redirect('/storage/');

        } catch (Exception $e) { 
            $this->page->view_exception($e);
            return;
        }
    }

}
