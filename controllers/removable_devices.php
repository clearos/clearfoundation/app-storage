<?php

/**
 * Removable devices controller.
 *
 * @category   apps
 * @package    storage
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Removable devices controller.
 *
 * @category   apps
 * @package    storage
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

class Removable_Devices extends ClearOS_Controller
{
    /**
     * Removable devices controller.
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage');

        // Load view data
        //---------------

        try {
            $data['devices'] = $this->storage_device->get_devices();
            $found = $this->storage_device->find_obvious_storage_device();

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }


        // Load the views
        //---------------

        $this->page->view_form('removable_devices/summary', $data, lang('storage_removable'));
    }

    /**
     * Storage detail view.
     *
     * @param string $source encoded source name
     *
     * @return view
     */

    function view($source)
    {
        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');
        $this->load->library('storage/Storage');

        // Load view data
        //---------------

        try {
            $data['device'] = base64_decode(strtr($source, '-_.', '+/='));
            $data['details'] = $this->storage_device->get_device_details($data['device']);
            $data['types'] = $this->device_partition->get_file_system_types();
            $data['partition_types'] = $this->device_partition->get_partition_types();
            $data['storage_base'] = $this->storage->get_base();

            // Set default
            $data['type'] = 'ext4';
            $data['partition_type'] = 'primary';

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }


        // Load the views
        //---------------

        $this->page->view_form('removable_devices/item', $data, lang('storage_removable'));
    }

    /**
    * Format partition View
    *
    * @param string $partition_id partition id
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function format_removable_device($partition_id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('storage');

        // Partition decode
        $partition_id_decoded = base64_decode(strtr($partition_id, '-_.', '+/='));
        $partition_with_type = explode("=", $partition_id_decoded);

        // Get device for back URl
        $device_name = preg_replace('/[0-9]/', '', $partition_with_type[0]);
        $device_encoded = strtr(base64_encode($device_name),  '+/=', '-_.');


        // confirm uri
        //---------------
        $confirm_uri = '/app/storage/removable_devices/format_device_partition/' . $partition_id;
        $cancel_uri = '/app/storage/removable_devices/view/'. $device_encoded;
        $device_name = $partition_with_type[0];

        $this->page->view_confirm(lang('storage_confirm_format_usb') . "<b> $device_name </b>?", $confirm_uri, $cancel_uri);
    }

    /**
    * Format partition
    *
    * @param string $partition_id partition id
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function format_device_partition($partition_id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');

        // partition decoded
        //---------------
        $partition_decoded = base64_decode(strtr($partition_id, '-_.', '+/='));

        try {
            $partition_with_type = explode("=", $partition_decoded);
            $partition = $partition_with_type[0];
            $type = $partition_with_type[1];

            // Get device for back URl
            $device_name = preg_replace('/[0-9]/', '', $partition);
            $device_name_encoded = strtr(base64_encode($device_name),  '+/=', '-_.');

            $this->device_partition->do_removable_device_format($partition, $type);
            redirect('/storage/removable_devices/view/'.$device_name_encoded);

        } catch (Exception $e) { 

            $this->page->view_exception($e);
            return;
        }
    }

    /**
     * Confirm Delete partition view.
     *
     * @param string $partition encoded partition name
     *
     * @return view
    */

    function confirm_delete($partition)
    {

        $device_decoded = base64_decode(strtr($partition, '-_.', '+/='));

        // confirm uri
        //---------------

        $device_with_id = explode("=", $device_decoded);
        $device_encoded = strtr(base64_encode($device_with_id[0]),  '+/=', '-_.');
        $confirm_uri = '/app/storage/removable_devices/delete_partition/' . $partition;
        $cancel_uri = '/app/storage/removable_devices/view/'.$device_encoded;

        $device_with_id = $device_with_id[0].''.$device_with_id[1];
        $items = array($device_with_id);

        $this->page->view_confirm_delete($confirm_uri, $cancel_uri, $items);
    }

    /**
     * Delete a partiton.
     *
     * @param string $partition encoded partition name
     *
     * @return redirect
    */

    function delete_partition($partition)
    {
        $device_decoded = base64_decode(strtr($partition, '-_.', '+/='));

        // Load libraries
        //---------------
        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');

        try {
            $device_with_id = explode("=", $device_decoded);

            if (!empty($device_with_id)) {
                $device_value = $device_with_id[0];
                $device_id = $device_with_id[1];

                $this->device_partition->delete_removable_device_partition($device_value, $device_id);
                $this->page->set_status_deleted();

                // Get device for back URl
                //------------------------
                $device_encoded = strtr(base64_encode($device_value),  '+/=', '-_.');
                redirect('/storage/removable_devices/view/'. $device_encoded);
            }

        } catch (Exception $e) { 
            $this->page->view_exception($e);
            return;
        }
    }
    
    /**
     * Creates a partiton.
     *
     * @param string $device encoded device name
     *
     * @return view
    */

    function create_partition($device)
    {

        $device_decoded = base64_decode(strtr($device, '-_.', '+/='));

        // Load libraries
        //---------------

        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');

        try {

            $data['device'] = base64_decode(strtr($device, '-_.', '+/='));
            $data['types'] = $this->device_partition->get_file_system_types_for_removable_device();
            $data['partition_types'] = $this->device_partition->get_partition_types();
            $data['details'] = $this->storage_device->get_device_details($data['device']);
            $data['remaining_space'] = $this->device_partition->get_remaining_space($data['device']);
            $data['label_type'] = $this->device_partition->get_device_label_type($data['device']);

            // Set default
            $data['type'] = 'fat32';
            $data['partition_type'] = 'primary';

            // Handle form submit
            //-------------------

            if ($this->input->post('submit')) {

                $partition_type = $this->input->post('partition_type');
                $file_type = $this->input->post('file_type');
                $partition_size = $this->input->post('partition_size');
                $is_first_partition = $this->input->post('first_partition');

                $this->device_partition->create_removable_device_partition($device_decoded, $partition_type, $file_type, $partition_size, $is_first_partition);
                
                redirect('/storage/removable_devices/view/'. $device);
                $this->page->set_status_added();

            }

        } catch (Exception $e) { 
            $this->page->view_exception($e);
            return;
        }
        // Load the views
        //---------------

        $this->page->view_form('removable_devices/create_partition', $data, lang('storage_store'));
    }

    /**
    * Get all mount points for partition
    *
    * @param string $partition_id partition id
    *
    * @return View
    * @throws Engine_Exception
    */

    function mount_points($partition_id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');

        $device_decoded = base64_decode(strtr($partition_id, '-_.', '+/='));

        try {
            
            // Get mount points
            //---------------
            $data['mount_points'] = $this->storage_device->get_bind_mount_points($device_decoded);
            $data['partition'] = $device_decoded;
            $data['device_name'] = preg_replace('/[0-9]/', '', $device_decoded);

        } catch (Exception $e) { 
            $this->page->view_exception($e);
            return;
        }

        // View
        //........

        $this->page->view_form('removable_devices/partition_mount_points', $data);
    }

    /**
    * Mount Partition
    *
    * @param string $partition_id partition id
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function mount($partition_id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');

        // Mount Partition
        //----------------------------------------------
        
        $partition_id_decoded = base64_decode(strtr($partition_id, '-_.', '+/='));

        // Get device for back URl
        $data['device'] = preg_replace('/[0-9]/', '', $partition_id_decoded);

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';
        $data['partition_id'] = $partition_id_decoded;

        if ($_POST) {
          
            $partition_id = $this->input->post('partition_id');
            $mount_point = $this->input->post('storage_path');

            try {
                $this->storage_device->do_mount_removable_device_partition($partition_id, $mount_point);

                $partition_encoded = strtr(base64_encode($partition_id),  '+/=', '-_.');
                redirect('/storage/removable_devices/mount_points/'. $partition_encoded);

            } catch (Exception $e) { 
                $this->page->view_exception($e);
                return;
            }

        }

        // View
        //........

        $this->page->view_form('removable_devices/folder', $data);
    }

    /**
    * Unmount partition View
    *
    * @param string $partition_id partition id
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function unmount_partition($partition_id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('storage');

        $partition_mount = base64_decode(strtr($partition_id, '-_.', '+/='));
        $partition_id_mount = explode("=", $partition_mount);
        $partition_encoded = strtr(base64_encode($partition_id_mount[0]),  '+/=', '-_.');

        // confirm uri
        //---------------
        $confirm_uri = '/app/storage/removable_devices/unmount/' . $partition_id;
        $cancel_uri = '/app/storage/removable_devices/mount_points/' .$partition_encoded;
        $partition_name = $partition_id_mount[0];

        $this->page->view_confirm(lang('storage_confirm_unmount_partition') . "<b> $partition_name </b>?", $confirm_uri, $cancel_uri);
    }

    /**
    * Unount Partition
    *
    * @param string $partition_id partition id
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function unmount($partition_id)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('storage');
        $this->load->library('storage/Storage_Device');
        $this->load->library('storage/Device_Partition');

        // Unount Partition
        //----------------------------------------------
        $partition_id_decoded = base64_decode(strtr($partition_id, '-_.', '+/='));
        $partition_id_mount = explode("=", $partition_id_decoded);
        $partition_encoded = strtr(base64_encode($partition_id_mount[0]),  '+/=', '-_.');
        
        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';
        
        try {

            $partition_id = $partition_id_mount[0];
            $mount_point = $partition_id_mount[1];

            $this->storage_device->do_unmount_removable_device_partition($partition_id, $mount_point);

            redirect('/storage/removable_devices/mount_points/'. $partition_encoded);

        } catch (Exception $e) { 
            $this->page->view_exception($e);
            return;
        }
    }
}
