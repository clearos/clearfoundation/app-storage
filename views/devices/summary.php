<?php

/**
 * Devices summary view.
 *
 * @category   apps
 * @package    storage
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('storage');

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(
    lang('storage_device'),
    lang('storage_model'),
    lang('storage_size'),
    lang('storage_in_use')
);

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$anchors = array();

?>
<style type="text/css">
    .btn-group .btn{ font-size: 0.8em; padding: 2px 10px; }
</style>
<?php
///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////


foreach ($devices as $device => $details) {
    $device_encoded = strtr(base64_encode($device),  '+/=', '-_.');
    $multi_device_encoded[] = strtr(base64_encode($device),  '+/=', '-_.');

    //encoded value with maklabe laix, amiga, bsd, dvh, gpt, mac, msdos, pc98, sun, loop
    $device_encoded_with_gpt = $device.'='.'gpt';
    $device_encoded_gpt = strtr(base64_encode($device_encoded_with_gpt),  '+/=', '-_.');
    $device_encoded_with_msdos = $device.'='.'msdos';
    $device_encoded_msdos = strtr(base64_encode($device_encoded_with_msdos),  '+/=', '-_.');

    // TODO: format maklabe types

/*    $device_encoded_with_loop = $device.'='.'loop';
    $device_encoded_loop = strtr(base64_encode($device_encoded_with_loop),  '+/=', '-_.');
    $device_encoded_with_aix = $device.'='.'aix';
    $device_encoded_aix = strtr(base64_encode($device_encoded_with_aix),  '+/=', '-_.');
    $device_encoded_with_amiga = $device.'='.'amiga';
    $device_encoded_amiga = strtr(base64_encode($device_encoded_with_amiga),  '+/=', '-_.');
    $device_encoded_with_bsd = $device.'='.'bsd';
    $device_encoded_bsd = strtr(base64_encode($device_encoded_with_bsd),  '+/=', '-_.');
    $device_encoded_with_dvh = $device.'='.'dvh';
    $device_encoded_dvh = strtr(base64_encode($device_encoded_with_dvh),  '+/=', '-_.');
    $device_encoded_with_mac = $device.'='.'mac';
    $device_encoded_mac = strtr(base64_encode($device_encoded_with_mac),  '+/=', '-_.');
    $device_encoded_with_pc98 = $device.'='.'pc98';
    $device_encoded_pc98 = strtr(base64_encode($device_encoded_with_pc98),  '+/=', '-_.');
    $device_encoded_with_sun = $device.'='.'sun';
    $device_encoded_sun = strtr(base64_encode($device_encoded_with_sun),  '+/=', '-_.');
*/
    // Skip removable drives
    if ($details['removable'])
        continue;

    // format of in use
    if ($details['in_use']) {
        $options = array('disabled' => 'disabled');
        $format_anchors = anchor_custom('/app/storage/devices/format_device/' . $device_encoded, lang('device_format'), 'high', $options);
    } else {
        $format_anchors = anchor_multi(
            array (
                '/app/storage/devices/format_device/' . $device_encoded_gpt.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_gpt'),
                '/app/storage/devices/format_device/' . $device_encoded_msdos.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_msdos')
                // TODO: format maklabe types
         /*       '/app/storage/devices/format_device/' . $device_encoded_loop.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_loop'),
                '/app/storage/devices/format_device/' . $device_encoded_aix.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_aix'),
                '/app/storage/devices/format_device/' . $device_encoded_amiga.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_amiga'),
                '/app/storage/devices/format_device/' . $device_encoded_bsd.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_bsd'),
                '/app/storage/devices/format_device/' . $device_encoded_dvh.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_dvh'),
                '/app/storage/devices/format_device/' . $device_encoded_mac.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_mac'),
                '/app/storage/devices/format_device/' . $device_encoded_pc98.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_pc98'),
                '/app/storage/devices/format_device/' . $device_encoded_sun.'' => lang('storage_format_by') . ': ' . lang('storage_format_by_sun'),*/
            ),
            lang('device_format')
        );

    }

    // TODO: discuss icon strategy
    $in_use_icon = ($details['in_use']) ? '<span class="fa fa-check">&nbsp;</span>' : '';
    $identifier = $details['identifier'];

    $item['title'] = $device;
    $item['action'] = '';
    $item['anchors'] = button_set(
        array(anchor_custom('/app/storage/devices/view/' . $device_encoded, lang('base_view_details')))
    );
    $item['details'] = array(
        $device,
        $identifier,
        $details['size'] . ' ' . $details['size_units'],
        $in_use_icon,
    );

    $items[] = $item;
}

sort($items);

// FIXME: Wizard
// in_use + is_store -> Storage is good to go!
// !in_use + !is_store -> Jump to create store view?
// in_use + !is_store -> No storage devices found, use directory on root partition?
// !in_use + is_store -> Storage not mounted ! 

///////////////////////////////////////////////////////////////////////////////
// Summary table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'storage_summary',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    lang('storage_devices'),
    $anchors,
    $headers,
    $items,
    $options
);
