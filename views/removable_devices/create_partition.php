<?php

/**
 * Create data drive view.
 *
 * @category   apps
 * @package    storage
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('storage');

///////////////////////////////////////////////////////////////////////////////
// Infobox
///////////////////////////////////////////////////////////////////////////////

echo infobox_warning('', lang('partition_create_info'));

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

$device_encoded = strtr(base64_encode($device),  '+/=', '-_.');

    echo form_open('storage/removable_devices/create_partition/' . $device_encoded);
    echo form_header(lang('create_partition'));

	echo fieldset_header('Device Details');
	echo field_view(lang('storage_device'), $device, lang('storage_size'));
	echo field_input('size', $details['size'] . ' ' . $details['size_units'], lang('storage_size'), TRUE);
    echo field_input('remaining_space', $remaining_space.' GB', 'Remaining Space', TRUE);
	echo field_input('identpifier', $details['identifier'], lang('storage_model'), TRUE);

    if ($label_type['device_label']) {
        echo field_input('device_label_type', $label_type['device_label'], lang('storage_device_label'), TRUE);
    }

	echo fieldset_footer();

	echo fieldset_header(lang('create_partition'));
    $count_partition = count($details['partitioning']['partitions']);
    if ($count_partition) {
        
        $first_partition = "<div style='display:none'>".field_input('first_partition', 1, "", TRUE)."</div>";
        echo $first_partition;
    }
    echo field_dropdown('partition_type', $partition_types, $partition_type, lang('partition_type'));
    echo field_dropdown('file_type', $types, $type, lang('storage_file_system'));
    echo field_input('partition_size', $remaining_space, lang('partition_size'), FALSE);


    echo field_button_set(
        array(
            form_submit_custom('submit', lang('base_create')),
            anchor_cancel('/app/storage/removable_devices/view/'.$device_encoded)
        )
    );

    echo form_footer();
    echo form_close();
