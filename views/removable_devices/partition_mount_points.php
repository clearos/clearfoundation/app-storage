<?php

/**
 *Partition mount points summary view.
 *
 * @category   apps
 * @package    storage
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('storage');

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('storage_mount_point'), lang('storage_file_system'));

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$partition_id_encode = strtr(base64_encode($partition),  '+/=', '-_.');
$device_encode = strtr(base64_encode($device_name),  '+/=', '-_.');


$anchors = array(anchor_custom('/app/storage/removable_devices/mount/' . $partition_id_encode, lang('storage_mount_new')), anchor_custom('/app/storage/removable_devices/view/'.$device_encode, lang('base_return_to_summary')));

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////


foreach ($mount_points as $point => $details) {

    $partition_id_point = $partition.'='.trim($details['mount_point']);
    $partition_point_encode = strtr(base64_encode($partition_id_point),  '+/=', '-_.');

    $point = trim($details['mount_point']);
    $file_system = $details['file_system'];

    $item['title'] = $partition;
    $item['action'] = '';
    $item['anchors'] = button_set(
        array(anchor_custom('/app/storage/removable_devices/unmount_partition/' . $partition_point_encode, lang('unmount')))
    );
    $item['details'] = array(
        $point,
        $file_system
    );

    $items[] = $item;
}

sort($items);

// FIXME: Wizard
// in_use + is_store -> Storage is good to go!
// !in_use + !is_store -> Jump to create store view?
// in_use + !is_store -> No storage devices found, use directory on root partition?
// !in_use + is_store -> Storage not mounted ! 

///////////////////////////////////////////////////////////////////////////////
// Summary table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'mount_points_summary',
   // 'responsive' => array(1 => 'none')
);

$title = lang('storage_partition_details'). $partition;
echo summary_table(
    $title,
    $anchors,
    $headers,
    $items,
    $options
);
